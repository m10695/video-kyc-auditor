import React, { useEffect } from 'react';
import { createPortal } from 'react-dom';
const $ = window.$;

const PortalReportModal = (props) => {
    useEffect(() => {
        // props?.isOpen ? $('#portalModal').modal('show') : $('#portalModal').modal('hide');
    }, [])
    const element = document.getElementById('portal-root');
    const customTag = props?.isOpen ? <>
        {/* <div className={`modal fade `}
            id="portalModal"
            tabIndex={-1}
            data-keyboard="false"
            data-backdrop="static"
            role="dialog"
            aria-labelledby="exampleModalLabel"
            style={{ display: 'block' }}
            aria-hidden="true">
            <div className={`${props?.isBottom ? 'custom-dialog-end ' : ''}modal-dialog modal-dialog-centered`} role="document">
                <div className="modal-content modal-form">
                    {props?.children}
                </div>
            </div>
        </div> */}


        <div className={`modal fade ${props?.isOpen && ' show'}`} id="viewKycReportModal"
        style={{ display: 'block' }}
         tabIndex={-1} aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div className="modal-dialog modal-xl modal-dialog-centered modal-dialog-centered modal-dialog-scrollable">
                <div className="modal-content">
                    {props?.children}
                </div>
            </div>
        </div>
        <div className="modal-backdrop fade show"></div>
    </> : (null)

    return createPortal(customTag, element)
}

export default PortalReportModal