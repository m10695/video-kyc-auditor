import React, { useEffect } from 'react';
import { createPortal } from 'react-dom';
const $ = window.$;

const PortalModal = (props) => {
    useEffect(() => {
        props?.isOpen ? $('#portalModal').modal('show') : $('#portalModal').modal('hide');
    }, [])
    const element = document.getElementById('portal-root');
    const customTag = props?.isOpen ? <>
        <div className={`modal fade ${props?.isOpen && ' show'}`}
            id="portalModal"
            tabIndex={-1}
            data-keyboard="false"
            data-backdrop="static"
            role="dialog"
            aria-labelledby="exampleModalLabel"
            style={{ display: 'block' }}
            aria-hidden="true">
            <div className={`${props?.isBottom ? 'custom-dialog-end ' : ''}modal-dialog modal-dialog-centered`} role="document">
                <div className="modal-content modal-form">
                    {props?.children}
                </div>
            </div>
        </div>
        <div className="modal-backdrop fade show"></div>
    </> : (null)

    return createPortal(customTag, element)
}

export default PortalModal