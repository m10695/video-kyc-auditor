import axios from "axios";

const instance = axios.create({
    baseURL: process.env.REACT_APP_BASE_URL
});

instance.defaults.headers.common['Content-Type'] = "application/json";
instance.defaults.headers.common['apikey'] = "0";
// instance.defaults.headers.common["authkey"] = "";
// instance.defaults.headers.common["authkey"] = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImFnZW50QHN5bnRpemVuLmNvbXwzfDF8SlVQVUItTU5ZT0ktR1FFRVotTkdTWUR8QWdlbnQiLCJuYmYiOjE2NDk2NTI1NTQsImV4cCI6MTY0OTczODk1NCwiaWF0IjoxNjQ5NjUyNTU0fQ.QDCsbEhJlHDe7QYDJslSOnk6qKx-S0ivWFzfWLPWTaM";
// instance.defaults.headers.common['Content-Type'] = 'multipart/form-data';

instance.interceptors.request.use(
    request => {
        const authkey = sessionStorage.getItem('authkey');
        // console.log(authkey, 'authkey')
        request.headers.common["authkey"] = authkey;
        // if (authkey) {
        // }

        return request;
    },
    error => {
        return Promise.reject(error);
    }
);


instance.interceptors.response.use(
    response => {
        return response;
    },
    error => {
        return Promise.reject(error);
    }
);


export default instance;