import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import { changePasswordSagaAction, UserLogoutSagaAction } from '../../Store/SagaActions/AuthSagaActions';
import toast from 'react-hot-toast';
import RouteNames from '../../Constants/RouteNames';
import { useNavigate } from 'react-router-dom';
import base64 from 'base-64';
import ProfileSettingsModal from '../Modals/ProfileSettingsModal'
import { useDispatch } from 'react-redux';
import { getUserProfileSagaAction } from '../../Store/SagaActions/CommonSagaActions';
import PortalReportModal from '../../Portals/PortalReportModal';
import ChangePasswordModal from '../Modals/ChangePasswordModal';


const Header = (props) => {

    const [showActiveDropdwon, setShowDropdonw] = useState(false);
    const [isOpen, setIsOpen] = useState(false);
    const [isOpenChangePassword, setIsOpenChangePassword] = useState(false);
    const [profileSettings, setProfileSettings] = useState([]);
    const [changePassInput, setChangePassInput] = useState({ current: "", newpassword: "", retypepassword: "" })

    const dispatch = useDispatch();
    const navigate = useNavigate();

    useEffect(() => {
        dispatch(getUserProfileSagaAction({ callback: fetchUserProfile }))
    }, [])

    const fetchUserProfile = (data) => {
        console.log(data, 'user profile')
        setProfileSettings(data);
    }

    const showDropDown = () => {
        setShowDropdonw(!showActiveDropdwon);
    }


    const toggleProfileDrop = () => {
        setIsOpen(!isOpen);
    }
    const toggleChangePasswordDrop = () => {
        setIsOpenChangePassword(!isOpenChangePassword)
    }

    const closeModal = () => {
        setIsOpen(!isOpen);
    }
    const closeModalChangePassword = () => {
        setIsOpenChangePassword(!isOpenChangePassword)
    }

    const handleChangePassword = (event) => {
        const { name, value } = event.target;
        setChangePassInput({
            ...changePassInput, [name]: value
        })
    }




    const submitChangePassword = () => {
        const model = {
            current_pwd: base64.encode(changePassInput.current),
            new_pwd: base64.encode(changePassInput.newpassword)
        }

        if (changePassInput.newpassword == changePassInput.retypepassword && changePassInput.current != changePassInput.newpassword) {
            dispatch(changePasswordSagaAction({ model: model, callback: submitChangePasswordResponse }))
        } else {
            toast.error('Password not matched')
        }

    }

    const submitChangePasswordResponse = (data) => {
        setIsOpenChangePassword(!isOpenChangePassword)

    }

    const logoutUser = () => {
        dispatch(UserLogoutSagaAction({callback: logoutUserResponse}))
    }

    const logoutUserResponse = (data) => {
        navigate(RouteNames.LOGIN)
    }

    return (
        <>
            <nav className="navbar navbar-expand-lg top-nav">
                <div className="container-fluid">
                    <a className="navbar-brand" href="#">
                        <img src="../images/logo-icon.svg" className='me-2' alt="logo" />
                        <span className='agnt-name'>
                            Good Evening {sessionStorage.getItem('username')}!
                        </span>
                    </a>
                    
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon" />
                    </button>
                    <div className="collapse navbar-collapse justify-content-end" id="navbarSupportedContentd">
                        {/* <ul className="navbar-nav mr-auto">
                            <li className="nav-item agnt-name">
                                Good Evening Aleesha!
                            </li>
                        </ul> */}

                        <div className="dropdown ar-auto">
                            <a className={`nav-link agnt-prf dropdown-toggle ${showActiveDropdwon ? "show" : ""}`} onClick={showDropDown} href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <div className="center">
                                    <img src="../images/profile-pic.svg" className="agnt-prf-img" alt="profile-img" />
                                    <div className="agnt-prf-bx">
                                        <h6 className="agnt-prf-name">{sessionStorage.getItem('username')}</h6>
                                        <p className="agnt-prf-subname">Auditor online</p>
                                    </div>
                                </div>
                            </a>
                            {/* <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a className="dropdown-item" href="#">Profile settings</a>
                                <a className="dropdown-item" href="#">Change password</a>
                                <a className="dropdown-item" href="#">Logout</a>
                            </div> */}
                            <ul className={`dropdown-menu custom-border ${showActiveDropdwon ? "show" : ""} `} onClick={showDropDown} aria-labelledby="navbarDropdown">
                                <li><a className="dropdown-item" href="#" onClick={toggleProfileDrop}><img src="../images/profile.svg" alt="" />
                                    <span>Profile setttings</span> </a></li>
                                <li><a className="dropdown-item" href="#" onClick={toggleChangePasswordDrop}><img src="../images/lock key alt.svg" alt="" />
                                    <span>Change password</span> </a></li>
                                <li><a className="dropdown-item" href="#" onClick={logoutUser}><img src="../images/Logout.svg" alt="" />
                                    <span>Logout</span> </a></li>
                            </ul>


                        </div>
                    </div>
                </div>
            </nav>

            <PortalReportModal isOpen={isOpen}>
                <ProfileSettingsModal closeModal={closeModal} profileSettings={profileSettings} />
            </PortalReportModal>

            <PortalReportModal isOpen={isOpenChangePassword}>
                <ChangePasswordModal submitChangePassword={submitChangePassword} closeModal={closeModalChangePassword} handleChangePassword={handleChangePassword} />
            </PortalReportModal>


        </>
    )
}

// Header.propTypes = {
//     title: PropTypes.string,
//     navigate: PropTypes.func,
// }

export default Header;