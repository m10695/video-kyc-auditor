import React, { useEffect, useState } from 'react'
import toast from 'react-hot-toast';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import base64 from 'base-64';
import LoginCmp from '../../Components/FormCmp/LoginCmp';
import RouteNames from '../../Constants/RouteNames';
import { getRolesSagaAction, loginSagaAction } from '../../Store/SagaActions/AuthSagaActions';

const Login = () => {
    const [loginObj, setLoginObj] = useState({
        username: '',
        password: '',
    });
    const [roleObj, setRoleObj] = useState({});
    const [roles, setRoles] = useState([]);

    const naviage = useNavigate();
    const dispatch = useDispatch();

    useEffect(() => {
        sessionStorage.clear();
        dispatch(getRolesSagaAction({ callback: getRolesData }));
    }, []);

    const getRolesData = (data) => {
        setRoles(data)
    }


    const handleChange = (e) => {
        const { name, value } = e.target;
        if (name === 'rolename') {
            const role = value.split('-')
            const roleObjData = {
                rid: role[0],
                rolename: role[1],
            }
            setRoleObj(roleObjData);
        } else {
            setLoginObj({ ...loginObj, [name]: value })
        }
    }

    const login = (e) => {
        e.preventDefault();
        sessionStorage.setItem('username', loginObj.username)
        if (loginObj.username && loginObj.password && roleObj?.rid && roleObj?.rolename) {
            const encodePwd = base64.encode(loginObj?.password);
            const model = {
                username: loginObj.username,
                password: encodePwd,
                rolename: roleObj?.rolename,
                rid: roleObj.rid,
                authflag: '1'
            }
            dispatch(loginSagaAction({ model: model, callback: loginRespData }))
        } else {
            toast.error('Please check all inputs')
        }
    }

    const loginRespData = (data) => {
        naviage(RouteNames.PENDING);
    }

    return (
        <LoginCmp
            roles={roles}
            handleChange={handleChange}
            login={login}
        />
    )
}

export default Login