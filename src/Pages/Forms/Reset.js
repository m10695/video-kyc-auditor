import React, { useEffect, useState } from 'react'
import ResetCmp from '../../Components/FormCmp/ResetCmp'
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import base64 from 'base-64';
import LoginCmp from '../../Components/FormCmp/LoginCmp';
import RouteNames from '../../Constants/RouteNames';
import { getRolesSagaAction, resetSagaAction } from '../../Store/SagaActions/AuthSagaActions';
import toast from 'react-hot-toast';


const Reset = () => {
    const [resetObj, setResetObj] = useState("");
    
    const naviage = useNavigate();
    const dispatch = useDispatch();

    // useEffect(() => {
    //     dispatch(getRolesSagaAction({ callback: getRolesData }));
    // }, []);

    // const getRolesData = (data) => {
    //     setRoles(data)
    // }
    const handleChange = (e) => {
        console.log(e.target.value,"e.target.value)")
        setResetObj(e.target.value);

    }

    const resetBtn = (e) => {
        e.preventDefault();
        if (resetObj) {
            const model = {
                "username":resetObj,
                "rid":"3"
            }
            console.log(resetObj,"mdoel")
            dispatch(resetSagaAction({ model: model, callback: resetRespData }))
        } else {
            toast.error('Please check all inputs')
        }
    }

    const resetRespData = (data) => {
        // naviage(RouteNames.LIVEDASHBOARD_LIVE);
        // toast.success('Successfully sent Reset link to the User email')


    }
    return (
        <>
            <ResetCmp 
                handleChange={handleChange}
                reset={resetBtn}
            />
        </>
    )
}

export default Reset