import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux';
// import { useNavigate } from 'react-router-dom';
import PendingCmp from '../../../Components/DashboardCmp/CustomerQueue/PendingCmp';
import PortalModalKycModal from '../../../Portals/PortalModalKycModal';
// import { getDashboardLiveSagaAction } from '../../../Store/SagaActions/AuditorDashboardSagaActions';
import { getDashboardPendingSagaAction, getDashboardVCIPDetailsSagaAction, getUpdateVCIPIDStatusByAuditorSagaAction } from '../../../Store/SagaActions/AuditorDashboardSagaActions'
import KycReport from '../../Modals/KycReport';
// import { joinVideoSessionAction } from '../../../Store/SagaActions/InitiateVideoCallSagaActions';

const Pending = () => {
    const [PendingList, setPendingList] = useState([]);
    const [state, setState] = useState(false);
    const [kycReportData, setKycReportData] = useState();
    // const [vcipID, setVcipID] = useState('');
    const [isIssueOpened, setIsIssueOpened] = useState(false);
    const [addRemark, setAddRemark] = useState("");
    const [getAuditorStatus, setgetAuditorStatus] = useState("");
    const [getVcipKey, setgetVcipKey] = useState("");
    const [disabled, setDisabled] = useState(true);

    const dispatch = useDispatch();
    let intervalId;
    // const navigate = useNavigate();
    // let vcipData;

    useEffect(() => {
        getPendingList();
        intervalId = setInterval(() => {
            getPendingList();
        }, 5000);
        return () => {
            clearIntervalLive();
        }
    }, []);

    const clearIntervalLive = () => {
        if (intervalId) {
            clearInterval(intervalId)
        }
    }

    const getPendingList = () => {
        const model = {
            listtype: '1' // -> 1-live, 2-scheduel
        }
        dispatch(getDashboardPendingSagaAction({ model: model, callback: getLiveData }));
    }

    const getLiveData = (data) => {
        // setPendingList(data);
        let pendingListData = []

        if (data?.length > 0) {
            data?.map((item, i) => {
                pendingListData.push({
                    "date of upload": item?.dateofupload, "customer name": item?.cusname, "app id": item?.appid, "verify agent": item?.verifyagent, "duration": item?.duration, "agent status": item?.agentstatus == "3" ? <span className='stat-bx rej' >Rejected </span>  : <span className='stat-bx suc' >Approved </span> , "button":
                        <button className='tab-btn' onClick={() => openModale(item?.vcipkey)}>view</button>
                })
                //         <button type="button" className="btn btn-primary w-20" data-bs-toggle="modal" data-bs-target="#viewKycReportModal">
                //             View Kyc Report
                //         </button>
                // })
            })



        } else {
            console.log(
                "lsdkfj"
            )

        }
        setPendingList(pendingListData)
    }

    // const joinVideoSession = (vcipData) => {
    //     const vcipID = vcipData?.videoconfsessionid;
    //     sessionStorage.setItem('vcipkey', vcipData?.vcipkey);
    //     const location = sessionStorage.getItem('location');
    //     const geolocation = sessionStorage.getItem('geolocation');
    //     const ip = sessionStorage.getItem('ip');
    //     const connection = navigator.connection || navigator.mozConnection || navigator.webkitConnection;
    //     const model = {
    //         "vcipkey": vcipData?.vcipkey,
    //         "custdetails": {
    //             "ip": "10.10.10.10",
    //             "location": "",
    //             "geolocation": "",
    //             "nw_incoming": "",
    //             "nw_outgoing": "",
    //             "videoresolution": ""
    //         },
    //         "agentdetails": {
    //             "ip": ip,
    //             "location": location,
    //             "geolocation": geolocation,
    //             "nw_incoming": connection.downlink,
    //             "nw_outgoing": `${connection.rtt}, ${connection.effectiveType === "4g" ? connection.effectiveType : "Low Speed"}`,
    //             "videoresolution": `${window.screen.width} * ${window.screen.height}`
    //         }

    //     }
    //     // dispatch(joinVideoSessionAction({ model: model, callback: joinVideoSessionData, vcipID: vcipID }));
    // }

    // const joinVideoSessionData = (vcipID) => {
    //     // if (intervalConferenceId) {
    //     //     clearInterval(intervalConferenceId)
    //     // }
    //     // sessionStorage.setItem('videoconfsessionid', InitiateVCFQ?.videoconfsessionid);
    //     // navigate(RouteNames.INITIATE_VIDEO_CALL + InitiateVCFQ?.videoconfsessionid);
    //     navigate('/video/' + vcipID);
    // }

    const openModale = (VcipKey) => {
        setgetVcipKey(VcipKey)
        const model = {
            vcipkey: VcipKey
        }
        dispatch(getDashboardVCIPDetailsSagaAction({ model: model, callback: getKycReport }))
    }

    const getKycReport = (kycreportdata) => {
        setIsIssueOpened(prevState => !prevState);

        setKycReportData(kycreportdata);
    }

    const toggleBtn = () => {
        setIsIssueOpened(prevState => !prevState);

    }

    const handleChangeMarkRemarks = (value) => {
        // console.log(value, 'adf')
        setAddRemark(value);
    }
    

    const handleChangeMarkText = (value) => {
        setDisabled(!disabled);
        setgetAuditorStatus(value)
    }

    const submitKycReport = () => {
        const model = {
            "vcipkey": getVcipKey,
            "vcipstatus": getAuditorStatus,
            "auditorstatus": getAuditorStatus == '4' ? "1" : "2",
            "remarks": addRemark
        }
        dispatch(getUpdateVCIPIDStatusByAuditorSagaAction({model: model, callback: sumbitedKycReport}))
    }
    
    const sumbitedKycReport = (value) => {
        setDisabled(!disabled);
        setgetAuditorStatus("");
        setAddRemark("");


        // console.log(value, 'value')
        setIsIssueOpened(prevState => !prevState);

    } 

    return (
        <>
            {/* <PendingCmp
                PendingList={PendingList}

                // joinVideoSession={joinVideoSession}
            /> */}
            <PendingCmp
                PendingList={PendingList}
                openModale={openModale}
                state={state}
                kycReportData={kycReportData}
                toggleBtn={toggleBtn}
            />

            {

                isIssueOpened && <PortalModalKycModal isOpen={true}>
                    <KycReport
                        kycReportData={kycReportData}
                        toggleBtn={toggleBtn}
                        handleChangeMarkRemarks={handleChangeMarkRemarks}
                        handleChangeMarkText={handleChangeMarkText}
                        submitKycReport={submitKycReport}
                        disable={disabled}

                    />
                </PortalModalKycModal>
            }


        </>
    )
}

export default Pending;