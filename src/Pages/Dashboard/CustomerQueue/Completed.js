import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux';
// import PendingCmp from '../../../Components/DashboardCmp/CustomerQueue/PendingCmp';
import CompletedCmp from '../../../Components/DashboardCmp/CustomerQueue/CompletedCmp';
// import { getDashboardCompletedSagaAction } from '../../../Store/SagaActions/AgentDashboardSagaActions';
import { getDashboardCompletedSagaAction, getDashboardVCIPDetailsSagaAction } from '../../../Store/SagaActions/AuditorDashboardSagaActions';
import PortalModalKycModalBack from '../../../Portals/PortalModalKycModalBack'
import KycReportBack from '../../Modals/KycReportBack';


const Completed = () => {
    const [completed, setCompleted] = useState([]);
    const [state, setState] = useState(false);
    const [kycReportData, setKycReportData] = useState();
    const [isIssueOpened, setIsIssueOpened] = useState(false);

    const dispatch = useDispatch();
    let intervalId;

    useEffect(() => {
        getCompleted();
        intervalId = setInterval(() => {
            getCompleted();
        }, 5000);
        return () => {
            clearIntervalCompleted();
        }
    }, []);

    const clearIntervalCompleted = () => {
        if (intervalId) {
            clearInterval(intervalId)
        }
    }

    const getCompleted = () => {
        const model = {
            listtype: '2' // -> 1-live, 2-scheduel
        }
        dispatch(getDashboardCompletedSagaAction({ model: model, callback: getCompletedData }));
    }

    const getCompletedData = (data) => {
        let completedListData = []

        // "dateofupload": "4/25/2022 3:05:38 PM",
        // "dateofaudit": "",
        // "vcipkey": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IjIyMTkwfDJ8MXxKVVBVQi1NTllPSS1HUUVFWi1OR1NZRHwzIiwibmJmIjoxNjQ5Njg1MjA1LCJleHAiOjE3MzU5OTg4MDUsImlhdCI6MTY0OTY4NTIwNX0.B3BWCBr_NHVnvqUYFwkblGoZS-TVn98gswaZYHwo2Wg",
        // "cusname": "uttam",
        // "appid": "1",
        // "verifyagent": "hareesh@syntizen.com",
        // "duration": "-00:01:43",
        // "agentstatus": "1",
        // "auditorstatus": "1"
        if (data?.length > 0) {
            data?.map((item, i) => {
                completedListData.push({
                    "date of upload": item?.dateofupload, "date of audit": item?.dateofaudit, "customer name": item?.cusname, "app id": item?.appid, "verify agent": item?.verifyagent, "duration": item?.duration, "agent status": item?.agentstatus == "3"
                        ? <span className='stat-bx rej' >Rejected </span>
                        // <button className="btn btn-red">Rejected</button>
                        : <span className='stat-bx suc' >Approved </span>, 
                        // <button className="btn btn-green">accepted</button>,
                    "auditor status": item?.auditorstatus == "2" ? <span className='stat-bx rej' >Rejected </span> : <span className='stat-bx suc' >Approved </span>, "button"
                        : <button className="tab-btn" onClick={() => openModale(item?.vcipkey)}>view</button>
                })
            })
        } else {
            console.log(
                "lsdkfj"
            )

        }
        setCompleted(completedListData);

    }

    const openModale = (VcipKey) => {
        const model = {
            vcipkey: VcipKey
        }
        dispatch(getDashboardVCIPDetailsSagaAction({ model: model, callback: getKycReport }))
    }

    const getKycReport = (kycreportdata) => {
        setState(!state)
        setIsIssueOpened(prevState => !prevState);
        setKycReportData(kycreportdata);
    }


    const toggleBtn = () => {
        setIsIssueOpened(prevState => !prevState);

    }

    return (
        <>
            <CompletedCmp
                completedList={completed}
                openModale={openModale}
                state={state}
                kycReportData={kycReportData}
                toggleBtn={toggleBtn}
            />


            {

                isIssueOpened && <PortalModalKycModalBack isOpen={true}>
                    <KycReportBack
                        kycReportData={kycReportData}
                        toggleBtn={toggleBtn}
                    // handleChangeMarkRemarks={handleChangeMarkRemarks}
                    // handleChangeMarkText={handleChangeMarkText}
                    // submitKycReport={submitKycReport}

                    />
                </PortalModalKycModalBack>
            }



        </>
    )
}

export default Completed;