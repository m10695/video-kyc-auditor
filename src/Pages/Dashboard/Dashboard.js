import React from 'react';
import { useSelector } from 'react-redux';
import { Outlet, useLocation, useNavigate } from 'react-router-dom';
import RouteNames from '../../Constants/RouteNames';

const Dashboard = () => {
    const count = useSelector(state => state.HomeReducer)

    const navigate = useNavigate();
    const location = useLocation();

    return (
        <>
            <div className="tab-pane fade show active" id="queue" role="tabpanel" aria-labelledby="queue-tab">
                <div className='d-flex align-items-center my-2'>
                    {/* <NavLink className={(navData) => navData?.isActive ? 'btn w-auto' : 'btn w-auto btn-white'} to='/dashboard/live'>Live</NavLink> */}
                    <button className={` ${location?.pathname === RouteNames.PENDING ? 'active' : 'btn-white'}`} onClick={() => navigate('/Auditor/pending')}>Pending ({count?.pendingListCount ? count?.pendingListCount : 0})</button>
                    <button className={` ${location?.pathname === RouteNames.COMPLETED ? 'active' : 'btn-white'}`} onClick={() => navigate('/Auditor/completed')}>Completed ({count?.completedListCount ? count?.completedListCount : 0})</button>
                    {/* <button className={`btn w-auto btn-white ${(navData) => navData?.isActive ? '' : 'btn-white'}`}>Assigned</button> */}
                </div>
                <Outlet />
            </div>

            {/*  className={`nav-link ${location?.pathname === RouteNames.CALLHISTORY ? 'active' : ''}`}
                                        onClick={() => navigate(RouteNames.CALLHISTORY)} */}


            <div className="tab-pane fade" id="history" role="tabpanel" aria-labelledby="history-tab">Coming Soon...!</div>
        </>
    )
}

export default Dashboard;