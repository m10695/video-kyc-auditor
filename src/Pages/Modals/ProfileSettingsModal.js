import React from 'react'

const ProfileSettingsModal = (props) => {
    {console.log(props?.profileSettings,"profile")}
    return (
        <>
            <div className="modal-header text-center">
                <h5 className="modal-title m-auto" id="exampleModalLabel">Profile settings</h5>
                {/* <button type="button" class="btn-close close" data-bs-dismiss="modal" aria-label="Close"></button> */}
                <img src="../images/Vector_close.svg" onClick={props?.closeModal} className="close" alt="" />
            </div>
            <div className="modal-body">
                <div className="row">
                    <div className="col-md-6">
                        <img src={props?.profileSettings?.pht} />
                        <h5 className="agent-name">{props?.profileSettings?.name}</h5>
                        <hr />
                        <div>
                            <h6 className="information-phone"><img src="../../images/phone.svg" /> {props?.profileSettings?.mobile}</h6>
                            <h6 className="information-phone"><img src="../../images/email.svg" /> {props?.profileSettings?.email}</h6>
                        </div>
                    </div>
                    <div className="col-md-6 general-information">
                        <h6 className=" information text-start">General Information</h6>
                        <hr />
                        <form action method>
                            <table>
                                <tbody>
                                    {/* <tr>
                        <td colspan="2"></td>
                      </tr> */}
                                    <tr>
                                        {/* <hr></hr> */}
                                        <td>
                                            <h6 className="first-name text-start">Manager ID:</h6>
                                        </td>
                                        <td>
                                            <h6 className="information">&nbsp;&nbsp;{props?.profileSettings?.manager_id}</h6>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h6 className="first-name text-start">Employee ID:</h6>
                                        </td>
                                        <td>
                                            <h6 className="information">&nbsp;&nbsp;{props?.profileSettings?.employeeid}</h6>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h6 className="first-name text-start">Location:</h6>
                                        </td>
                                        <td>
                                            <h6 className="information">&nbsp;&nbsp;{props?.profileSettings?.location}</h6>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h6 className="first-name text-start"> Region:</h6>
                                        </td>
                                        <td>
                                            <h6 className="information">&nbsp;&nbsp;{props?.profileSettings?.region}</h6>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h6 className="first-name text-start"> Branch ID:</h6>
                                        </td>
                                        <td>
                                            <h6 className="information">&nbsp;&nbsp;{props?.profileSettings?.branchid}</h6>
                                        </td>
                                    </tr>
                                    {/* <tr>
                                        <td>
                                            <h6 className="first-name text-start"> Work Plan:</h6>
                                        </td>
                                        <td>
                                            <h6 className="information">&nbsp;&nbsp;{props?.profileSettings?.employeeid}</h6>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h6 className="first-name text-start"> Office Timings:</h6>
                                        </td>
                                        <td>
                                            <h6 className="information">&nbsp;&nbsp;</h6>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h6 className="first-name text-start">Break Timings 1:</h6>
                                        </td>
                                        <td>
                                            <h6 className="information">&nbsp;&nbsp;</h6>
                                        </td>
                                    </tr> */}
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </>

    )
}

export default ProfileSettingsModal;