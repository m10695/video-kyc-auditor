import React, { useState } from 'react'

function KycReport(props) {

    let reportData = props?.kycReportData

    return (
        <>
            <div className="modal-header">
                <h5 className="modal-title m-auto" id="exampleModalLabel">
                    KYC Report </h5>
                <button type="button" className="close" data-bs-dismiss="modal" aria-label="Close">
                    <i className="fa-solid fa-xmark" />
                </button>
            </div>
            <div className="modal-body">
                <div className="row ">
                    <div className="col-lg-12">
                        <div className="details-head">
                            Customer Details
                        </div>
                        <table className="table table-borderless">
                            <thead>
                                <tr>
                                    <th scope="col">User details</th>
                                    <th scope="col">Applicant from data</th>
                                    <th scope="col">Aadhaar data
                                        <button className="btn btn-success btn-circle btn-circle-sm m-1">
                                            <img src="../images/Approve(Tick).svg" alt="" /></button></th>
                                    <th scope="col">PAN details
                                        <button className="btn btn-success btn-circle btn-circle-sm m-1">
                                            <img src="../images/Approve(Tick).svg" alt="" />
                                        </button></th>
                                    <th scope="col">Match</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td className="kyc-reportdataheader">
                                        <p>Name</p>
                                        <p>Father’s name
                                        </p>
                                        <p>Date of birth</p>
                                        <p>Gender
                                        </p>
                                        <p>Current address</p>
                                        <p>Permanent address</p>
                                        <p>Mobile number</p>
                                        <p>Email address</p>
                                    </td>
                                    <td className="kyc-reportdata">
                                        <p className="kyc-data">{reportData?.customerdetails?.name ? reportData?.customerdetails?.name : "-"}</p>
                                        <p className="kyc-data">{reportData?.customerdetails?.fname ? reportData?.customerdetails?.fname : "-"}</p>
                                        <p className="kyc-data">{reportData?.customerdetails?.dob ? reportData?.customerdetails?.dob : "-"}</p>
                                        <p className="kyc-data">{reportData?.customerdetails?.gender ? reportData?.customerdetails?.gender : "-"}</p>
                                        <p className="kyc-data">{reportData?.customerdetails?.curr_address ? reportData?.customerdetails?.curr_address : "-"}</p>
                                        <p className="kyc-data">{reportData?.customerdetails?.per_address ? reportData?.customerdetails?.per_address : "-"}</p>
                                        <p className="kyc-data">{reportData?.customerdetails?.mobile ? reportData?.customerdetails?.mobile : "-"}</p>
                                        <p className="kyc-data">{reportData?.customerdetails?.email ? reportData?.customerdetails?.email : "-"}</p>
                                    </td>
                                    <td className="kyc-reportdata">
                                        <p className="kyc-aadhar">{reportData?.kycdetails?.name ? reportData?.kycdetails?.name : "-"}</p>
                                        <p className="kyc-aadhar">{reportData?.kycdetails?.fname ? reportData?.kycdetails?.fname : "-"}</p>
                                        <p className="kyc-aadhar">{reportData?.kycdetails?.dob ? reportData?.kycdetails?.dob : "-"}</p>
                                        <p className="kyc-aadhar">{reportData?.kycdetails?.gender ? reportData?.kycdetails?.gender : "-"}</p>
                                        <p className="kyc-aadhar">{reportData?.kycdetails?.address ? reportData?.kycdetails?.address : "-"}</p>
                                    </td>
                                    <td className="kyc-reportdata">
                                        <p className="kyc-pan">{reportData?.pandetails?.ainame ? reportData?.pandetails?.ainame : "-"}</p>
                                        <p className="kyc-pan">{reportData?.pandetails?.aifname ? reportData?.pandetails?.aifname : "-"}</p>
                                        <p className="kyc-pan">{reportData?.pandetails?.aidob ? reportData?.pandetails?.aidob : "-"}</p>
                                    </td>
                                    <td className="kyc-reportdata">
                                        <p className="kyc-match text-success">{reportData?.fuzzymatchdetails?.kyc_pan_name ? reportData?.fuzzymatchdetails?.kyc_pan_name : '-'}</p>
                                        <p className="kyc-match text-success">{reportData?.fuzzymatchdetails?.kyc_pan_fname ? reportData?.fuzzymatchdetails?.kyc_pan_fname : '-'}</p>
                                        <p className="kyc-match text-success">{reportData?.fuzzymatchdetails?.kyc_pan_dob ? reportData?.fuzzymatchdetails?.kyc_pan_dob : '-'}</p>
                                        <p className="kyc-match text-success">{reportData?.fuzzymatchdetails?.kyc_gender ? reportData?.fuzzymatchdetails?.kyc_gender : '-'}</p>
                                        <p className="kyc-match text-success">{reportData?.fuzzymatchdetails?.kyc_curr_address ? reportData?.fuzzymatchdetails?.kyc_curr_address : '-'}</p>
                                        <p className="kyc-match text-success">{reportData?.fuzzymatchdetails?.kyc_per_address ? reportData?.fuzzymatchdetails?.kyc_per_address : '-'}</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div className="col-lg-6 mt-1 ">
                        <div className="face-box">
                            <h6 className="text-center">Face Match With Aadhaar</h6>
                            <div className="line">
                            </div>
                            <div className="text-center">
                                <img src={"data:image/png;base64," + reportData?.livecapturedetails?.livecapturepht} alt="live photo" />
                                <img src={"data:image/png;base64," + reportData?.kycdetails?.pht} alt="" />
                            </div>
                            <div className="line">
                            </div>
                            <div className="row">
                                <div className="col-lg-6 text-center">
                                    {/* <h5>Match score - <b className="text-success">99.9% </b></h5> */}
                                    <h5>Match score - <b className="text-success"> {reportData?.live_aadhaar_pht_matchlevel ? reportData?.live_aadhaar_pht_matchlevel : '-'}</b> </h5>

                                </div>
                                <div className="col-lg-6 text-center">
                                    <h5>Status - <b className="text-success">{reportData?.live_aadhaar_pht_matchstatus ? reportData?.live_aadhaar_pht_matchstatus : '-'} </b></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-6 mt-1 ">
                        <div className="face-box">
                            <h6 className="text-center">Face Match With PAN</h6>
                            <div className="line">
                            </div>
                            <div className="text-center">
                                <img src={"data:image/png;base64," + reportData?.livecapturedetails?.livecapturepht} alt="" />
                                <img src={"data:image/png;base64," + reportData?.pandetails?.pancard} alt="" />
                            </div>
                            <div className="line">
                            </div>
                            <div className="row">
                                <div className="col-lg-6 text-center">
                                    <h5>Match score - <b className="text-success">{reportData?.live_pan_pht_matchstatus ? reportData?.live_pan_pht_matchstatus : '-'} </b></h5>
                                </div>
                                <div className="col-lg-6 text-center">
                                    <h5>Status - <b className="text-success">{reportData?.live_pan_pht_matchlevel ? reportData?.live_pan_pht_matchlevel : '-'} </b></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-6 mt-3 ">
                        <div className="match-box">
                            <h6 className="text-center">Location Check</h6>
                            <div className="line">

                            </div>
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3805.3327409849744!2d78.39107421446921!3d17.491622688016285!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf379ac3f501b287e!2zMTfCsDI5JzI5LjgiTiA3OMKwMjMnMzUuOCJF!5e0!3m2!1sen!2sin!4v1650608549347!5m2!1sen!2sin" width={400} height={300} style={{ border: 0 }} allowFullScreen loading="lazy" referrerPolicy="no-referrer-when-downgrade" />
                            <div className="text-center">
                                <img src="images/no-map.jpg" alt="" />
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-6 mt-3 ">
                        <div className="match-box">
                            <h6 className="text-center">Liveness Check</h6>
                            <div className="line">
                            </div>
                            <div className="text-center">
                                <img src="images/no-map.jpg" alt="" />
                            </div>
                        </div>
                    </div>

                    <div className="row verify-auditor">
                        <div className="col-md-7 verify-auditor-reject">
                            <table className="table facematch-details">
                                <tbody><tr>
                                    <td>
                                        <h6>Verifying auditor status - <span className="text-danger approved-message">{reportData?.auditorstatus == "1" ?<span className="text-success approved-message">Approved</span> : reportData?.auditorstatus == "2" ? <span className="text-danger approved-message">Rejected</span> : "-"}</span></h6>
                                        <hr className="facehorizontal-row" />
                                        <div className="verify-status">
                                            <h3>Audited On: {reportData?.auditedon ? reportData?.auditedon : '-'}</h3>
                                            <h3>Audited By: {reportData?.auditoruserid ? reportData?.auditoruserid : '-'}</h3>
                                            <h3>Auditor Remarks: {reportData?.auditorremarks ? reportData?.auditorremarks : '-'}</h3>
                                        </div>
                                    </td>
                                </tr>
                                </tbody></table>
                            <table className="table facematch-details">
                                <tbody><tr>
                                    <td>
                                        <h6>Verifying agent’s status - <span className="text-success approved-message">{reportData?.agentstatus ? reportData?.agentstatus : '-'}</span></h6>
                                        <hr className="facehorizontal-row" />
                                        <div className="verify-status">
                                            <h3>Agent: {reportData?.agentuserid ? reportData?.agentuserid : '-'}</h3>
                                        </div>
                                    </td>
                                </tr>
                                </tbody></table>
                        </div>
                        <div className="col-md-5 Browser-details">
                            <table className="table facematch-details">
                                <tbody>
                                    <tr>
                                        <td>
                                            <h6>Browser &amp; IP details</h6>
                                            <hr className="facehorizontal-row" />
                                            <div className="ip-details">
                                                <h3>IP: {reportData?.customerdetails?.ipaddress ? reportData?.customerdetails?.ipaddress : "-"}</h3>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="agent-recording">
                        <div className="row agent-screen-recording">
                            <div className="col-md-7">
                                <table className="table facematch-details">
                                    <tbody><tr>
                                        <td>
                                            <div className="kycreport-header">
                                                <h6 className="report-video">Agent Screen Recording- 05 : 01 mins </h6>
                                                <span><img src="../images//Download.svg" />
                                                </span>
                                            </div>
                                            <hr className="facehorizontal-row" />
                                            <video width="100%" height={300} controls className="p-3">
                                                <source src={reportData?.videoconfdetails?.videolink ? reportData?.videoconfdetails?.videolink : '-'} type="video/mp4" />
                                                This browser doesn't support video tag.
                                            </video>
                                        </td>
                                    </tr>
                                    </tbody></table>
                            </div>
                            <div className="col-md-5 verify-agent-remark">
                                <table className="table facematch-details">
                                    <tbody><tr>
                                        <td>
                                            <h6>Verifying Agent's Remarks</h6>
                                            <hr className="facehorizontal-row" />
                                            <div className="remark-agent">
                                                <h3>Agent Remarks: {reportData?.agentremarks ? reportData?.agentremarks : '-'}</h3>

                                            </div>
                                        </td>
                                    </tr>
                                    </tbody></table>
                            </div>
                        </div>
                    </div>

                    {/* <div className="mark-box mt-3">
                        <input type="text" placeholder="Add remarks (optional)" onChange={(e) => props?.handleChangeMarkRemarks(e.target.value)} />
                        <div className="mark-text">
                            Mark the status of the application
                            <i className="fa-solid fa-x" onClick={() => props?.handleChangeMarkText("5")}/>
                            <i className="fa-solid fa-check" onClick={() => props?.handleChangeMarkText("4")} />
                        </div>
                    </div> */}
                </div>
                <div className="line mb-2">
                </div>
            </div>
            <div className="modal-footer footer-border m-auto text-center">
                {/* <button type="button" className data-bs-dismiss="modal" onClick={props?.toggleBtn}>
                    Cancel
                </button> */}
                <button type="button" className="active" data-dismiss="modal" onClick={props?.toggleBtn}>
                    go Back
                </button>
            </div>

        </>
    )
}

export default KycReport