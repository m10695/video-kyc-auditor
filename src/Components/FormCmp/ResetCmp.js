import React from 'react'
import { Link } from 'react-router-dom';
import RouteNames from '../../Constants/RouteNames';

const ResetCmp = (props) => {
    return (
        <form className="lgn-rt-frm" onSubmit={props?.reset}>
            <h1 className="lgn-rt-frm-ttl">Reset Password 🔐</h1>
            <p className="lgn-rt-frm-txt">Please enter your email address</p>
            <div className="frm-grp">
                <input type="email" name="email" className="frm-grp-inp" placeholder="Email Address" onChange={props?.handleChange}
                    required />
            </div>
            <div className="text-center">
                <button className="btn" type="submit" data-toggle="modal" data-target="#reset">Submit</button>
            </div>
            <p className="text-center mb-0 mt-2">
                <Link to={RouteNames.LOGIN} className="lgn-link">Login</Link>
            </p>
        </form>

    )
}

export default ResetCmp;