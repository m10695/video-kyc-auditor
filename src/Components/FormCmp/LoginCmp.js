import React from 'react'
import { Link } from 'react-router-dom'
import RouteNames from '../../Constants/RouteNames'

const LoginCmp = (props) => {
    return (
        <form className="lgn-rt-frm" onSubmit={props?.login}>
            <h1 className="lgn-rt-frm-ttl">Hello Again! 👋🏼</h1>
            <p className="lgn-rt-frm-txt">Welcome Back</p>
            <div className="frm-grp">
                <input
                    type="email"
                    name="username"
                    onChange={props?.handleChange}
                    className="frm-grp-inp"
                    placeholder="Email Address"
                    required />
            </div>
            <div className="frm-grp">
                <input
                    type="password"
                    name="password"
                    onChange={props?.handleChange}
                    className="frm-grp-inp"
                    placeholder="Password"
                    required />
            </div>
            <div className="frm-grp">
                <select
                    name='rolename'
                    onChange={props?.handleChange}
                    className="frm-grp-inp"
                    required>
                    {props?.roles && props?.roles?.length > 0
                        ? props?.roles.map((item, i) => <option key={i} value={item?.rid + '-' + item?.rolename}>{item?.rolename}</option>)
                        : (null)
                    }
                </select>
            </div>
            <div className="text-center">
                <button className="btn" type="submit">Login</button>
            </div>
            <p className="text-center mb-0 mt-2">
                <Link to={RouteNames.RESET} className="lgn-link">Forgot Password?</Link>
            </p>
        </form>

    )
}

export default LoginCmp