import React from 'react'
import StatusCodes from '../../../Constants/StatusCodes';

const PendingCmp = (props) => {
    let reportData = props?.kycReportData

    

    return (
        <>
            <table className="table">
                <thead className="text-center">
                    <tr>
                        <th scope="col">Date of upload</th>
                        <th scope="col">Customer Name</th>
                        <th scope="col">App ID</th>
                        <th scope="col">Verify agent</th>
                        <th scope="col">Duration</th>
                        <th scope="col">Agent Status</th>
                        <th scope="col">View</th>
                    </tr>
                </thead>
                <tbody className="text-center">
                    {props?.PendingList?.length > 0
                        ? props?.PendingList?.map((item, i) => <tr key={i}>
                            <td>{item?.dateofupload}</td>
                            <td>{item?.cusname}</td>
                            <td>{item?.appid}</td>
                            <td>{item?.verifyagent}</td>
                            <td>{item?.duration}</td>
                            {/* <td>{item?.agentstatus}</td> */}
                            <td>{item?.agentstatus == "1" ? <button className="approved-btn">Approved</button> : item?.agentstatus == "2" ? <button>Rejected</button> : <button>Issued</button>}</td>
                            {/* <td>{item?.auditorstatus == "1" ? <button className='approved-btn'>Approved</button> : item?.auditorstatus == "2" ? <button>Rejected</button> : <button>Issued</button>}</td> */}
                            <td><button className="approved-btn" onClick={() => props.openModale(item?.vcipkey)}>View</button></td>

                            {/* <td>
                            {item?.joinstatus === StatusCodes.VCIP_JOINSTATUS_ENABLE
                                ? <button className='btn' onClick={() => props?.joinVideoSession(item)}>Join Now</button>
                                : <button className='btn' disabled>Join Now</button>}

                        </td> */}
                        </tr>)
                        : (<tr><td colSpan={6} className='text-center'>No VCIPID's Found.</td></tr>)}
                </tbody>
            </table>

            {
                props.state &&

                <div className="container-fluid kyc-report">
                    <div className="card">
                        <div className="kycreport-header">
                            <h1 className="heading-kycreport">KYC Report </h1>
                            <span><img src="../images/filesave.svg" /></span>
                        </div>
                        <div style={{ overflowX: 'auto' }}>
                            <table className="table kycdetails">
                                <thead>
                                    <tr>
                                        <th colSpan={5} className="text-center">
                                            <h3 className="customer-heading">Customer details</h3>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th className="kyc-header">User details</th>
                                        <th className="kyc-header">Applicant from data</th>
                                        <th className="kyc-header">Aadhaar data <button className="btn btn-success btn-circle btn-circle-sm m-1">
                                            <img src="../images/Approve(Tick).svg" alt="" />
                                        </button></th>
                                        <th className="kyc-header">PAN details<button className="btn btn-success btn-circle btn-circle-sm m-1">
                                            <img src="../images/Approve(Tick).svg" alt="" />
                                        </button></th>
                                        <th className="kyc-header">Match</th>
                                    </tr>
                                </thead>
                                <tbody className="table-userdetails">
                                    <tr>
                                        <td className="kyc-reportdataheader">
                                            <p>Name</p>
                                            <p>Father’s name
                                            </p>
                                            <p>Date of birth</p>
                                            <p>Gender
                                            </p>
                                            <p>Current address</p>
                                            <p>Permanent address</p>
                                            <p>Mobile number</p>
                                            <p>Email address</p>
                                        </td>
                                        <td className="kyc-reportdata">
                                            <p className="kyc-data">{reportData?.customerdetails?.name}</p>
                                            <p className="kyc-data">{reportData?.customerdetails?.fname}</p>
                                            <p className="kyc-data">{reportData?.customerdetails?.dob}</p>
                                            <p className="kyc-data">{reportData?.customerdetails?.gender}</p>
                                            <p className="kyc-data">{reportData?.customerdetails?.curr_address}</p>
                                            <p className="kyc-data">{reportData?.customerdetails?.per_address}</p>
                                            <p className="kyc-data">{reportData?.customerdetails?.mobile}</p>
                                            <p className="kyc-data">{reportData?.customerdetails?.email}</p>
                                        </td>
                                        <td className="kyc-reportdata">
                                            <p className="kyc-aadhar">{reportData?.kycdetails?.name}</p>
                                            <p className="kyc-aadhar">{reportData?.kycdetails?.fname}</p>
                                            <p className="kyc-aadhar">{reportData?.kycdetails?.dob}</p>
                                            <p className="kyc-aadhar">{reportData?.kycdetails?.gender}</p>
                                            <p className="kyc-aadhar">{reportData?.kycdetails?.address}</p>
                                        </td>
                                        <td className="kyc-reportdata">
                                            <p className="kyc-pan">{reportData?.pandetails?.ainame}</p>
                                            <p className="kyc-pan">{reportData?.pandetails?.aifname}</p>
                                            <p className="kyc-pan">{reportData?.pandetails?.aidob}</p>
                                        </td>
                                        <td className="kyc-reportdata">
                                            <p className="kyc-match text-success">{reportData?.fuzzymatchdetails?.kyc_pan_name}</p>
                                            <p className="kyc-match text-success">{reportData?.fuzzymatchdetails?.kyc_pan_fname}</p>
                                            <p className="kyc-match text-success">{reportData?.fuzzymatchdetails?.kyc_pan_dob}</p>
                                            <p className="kyc-match text-success">{reportData?.fuzzymatchdetails?.kyc_gender}</p>
                                            <p className="kyc-match text-success">{reportData?.fuzzymatchdetails?.kyc_curr_address}</p>
                                            <p className="kyc-match text-success">{reportData?.fuzzymatchdetails?.kyc_per_address}</p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div className="row face-match-bx">
                            <div className="col-lg-4 fm-card">
                                <h1 className="face-match-heading">Face match with Aadhaar</h1>
                                <hr />
                                <div className="row">
                                    <div className="col-lg-6 fm-bx-img"><img src={"data:image/png;base64," + reportData?.livecapturedetails?.livecapturepht} alt="live photo" /></div>
                                    <div className="col-lg-6 fm-bx-img"><img src={"data:image/png;base64," + reportData?.kycdetails?.pht} alt="" /></div>
                                </div>
                                <hr />
                                <div className="d-flex justify-content-between">
                                    <h3>Match score - {reportData?.live_aadhaar_pht_matchlevel} </h3>
                                    <h3>Status - {reportData?.live_aadhaar_pht_matchstatus}</h3>
                                </div>
                            </div>
                            <div className="col-lg-4 fm-card">
                                <h1 className="face-match-heading">Face match with PAN</h1>
                                <hr />
                                <div className="row">
                                    <div className="col-lg-6 fm-bx-img"><img src={"data:image/png;base64," + reportData?.livecapturedetails?.livecapturepht} alt="live photo" /></div>
                                    <div className="col-lg-6 fm-bx-img"><img src={"data:image/png;base64," + reportData?.pandetails?.pancard} alt="" /></div>
                                </div>
                                <hr />
                                <div className="d-flex justify-content-between">
                                    <h3>Match score - {reportData?.live_pan_pht_matchstatus}</h3>
                                    <h3>Status - {reportData?.live_pan_pht_matchlevel}</h3>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-7">
                                <table className="table facematch-details">
                                    <tbody><tr>
                                        <td>
                                            <h6>Location Check</h6>
                                            <hr className="facehorizontal-row" />
                                            <div className="liveness-content">
                                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3805.3327409849744!2d78.39107421446921!3d17.491622688016285!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf379ac3f501b287e!2zMTfCsDI5JzI5LjgiTiA3OMKwMjMnMzUuOCJF!5e0!3m2!1sen!2sin!4v1650608549347!5m2!1sen!2sin" width={400} height={300} style={{ border: 0 }} allowFullScreen loading="lazy" referrerPolicy="no-referrer-when-downgrade" />
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody></table>
                            </div>
                            <div className="col-md-5">
                                <table className="table facematch-details">
                                    <tbody><tr>
                                        <td>
                                            <h6>livelinesscheck</h6>
                                            <hr className="facehorizontal-row" />
                                            <div className="liveness-content" />
                                        </td>
                                    </tr>
                                    </tbody></table>
                            </div>
                        </div>
                        <div className="row verify-auditor">
                            <div className="col-md-7 verify-auditor-reject">
                                <table className="table facematch-details">
                                    <tbody><tr>
                                        <td>
                                            <h6>Verifying auditor status - <span className="text-danger approved-message">{reportData?.auditorstatus}</span></h6>
                                            <hr className="facehorizontal-row" />
                                            <div className="verify-status">
                                                <h3>Audited On: {reportData?.auditedon}</h3>
                                                <h3>Audited By: {reportData?.auditoruserid}</h3>
                                                <h3>Auditor Remarks: {reportData?.auditorremarks}</h3>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody></table>
                                <table className="table facematch-details">
                                    <tbody><tr>
                                        <td>
                                            <h6>Verifying agent’s status - <span className="text-success approved-message">{reportData?.agentstatus}</span></h6>
                                            <hr className="facehorizontal-row" />
                                            <div className="verify-status">
                                                <h3>Agent: {reportData?.agentuserid}</h3>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody></table>
                            </div>
                            <div className="col-md-5 Browser-details">
                                <table className="table facematch-details">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <h6>Browser &amp; IP details</h6>
                                                <hr className="facehorizontal-row" />
                                                <div className="ip-details">
                                                    <h3>IP: {reportData?.customerdetails?.ipaddress}</h3>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        {/* Recording */}
                        <div className="agent-recording">
                            <div className="row agent-screen-recording">
                                <div className="col-md-7">
                                    <table className="table facematch-details">
                                        <tbody><tr>
                                            <td>
                                                <div className="kycreport-header">
                                                    <h6 className="report-video">Agent Screen Recording- 05 : 01 mins </h6>
                                                    <span><img src="../images//Download.svg" />
                                                    </span>
                                                </div>
                                                <hr className="facehorizontal-row" />
                                                <video width="100%" height={300} controls className="p-3">
                                                    <source src={reportData?.videoconfdetails?.videolink} type="video/mp4" />
                                                    This browser doesn't support video tag.
                                                </video>
                                            </td>
                                        </tr>
                                        </tbody></table>
                                </div>
                                <div className="col-md-5 verify-agent-remark">
                                    <table className="table facematch-details">
                                        <tbody><tr>
                                            <td>
                                                <h6>Verifying Agent's Remarks</h6>
                                                <hr className="facehorizontal-row" />
                                                <div className="remark-agent">
                                                    <h3>Agent Remarks: {reportData?.agentremarks}</h3>

                                                </div>
                                            </td>
                                        </tr>
                                        </tbody></table>
                                </div>
                            </div>
                        </div>
                        <div>
                            <input type="text" className=" w-75 form-control reset-input mx-auto remark-textarea" placeholder="Add remarks (optional)" />
                        </div>
                        <div className="questions">
                            Mark the status of the application
                            {/* <button className="btn btn-danger1 btn-circle btn-circle-sm m-1">
                                 <i className="fa fa-times text-white" aria-hidden="true" />
                            </button> */}
                            <button className="btn btn-danger1 btn-circle-sm m-1">
                                <img src="../images/Approve(Tick).svg" alt="" />
                            </button>
                            <button className="btn btn-success1 btn-circle-sm m-1">
                                <img src="../images/Approve(Tick).svg" alt="" />
                            </button>

                        </div>
                        <button onClick={props?.toggleBtn} type="button" className="submitbutton btn btn-success btn-circle btn-circle-sm" data-toggle="modal" data-target="#examplemodelfacematch">go Back</button>
                        {/* button using another page */}
                        {/* <button type ="button" class="submitbutton" data-toggle="modal" data-target="#examplemodelfacematch">Go back</button> */}
                    </div>
                    <div className="modal fade" id="examplemodelfacematch" tabIndex={-1} role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div className="modal-dialog modal-dialog-centered" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title" id="exampleModalLongTitle">Face match with Aadhaar
                                    </h5>
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true" className="text-secondary">×</span>
                                    </button>
                                </div>
                                <div className="modal-body modal-text">
                                    <div className="row">
                                        <div className="col-md-6 text-center pan-image-alignment ">
                                            <img src="../images/Rectangle 34.svg" alt="no img" className="modal-image" />
                                        </div>
                                        <div className="col-md-6 text-center pan-image-alignment2 ">
                                            <img src="../images/Rectangle 34.svg" alt="no img" className="modal-image" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }
        </>
    )
}

export default PendingCmp;