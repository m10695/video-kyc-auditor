import React, { Suspense } from 'react';
import { Toaster } from 'react-hot-toast';
import { useSelector } from 'react-redux';
import { Outlet, Route, Routes } from 'react-router-dom';
import DashboardLayout from './Layout/DashboardLayout';
import Dashboard from './Pages/Dashboard/Dashboard';
// import DashboardLayout from './Layout/DashboardLayout';
// import LoginLayout from './Layout/LoginLayout';
import Pending from './Pages/Dashboard/CustomerQueue/Pending';
import Completed from './Pages/Dashboard/CustomerQueue/Completed';
// import Dashboard from './Pages/Dashboard/Dashboard';
// import Login from './Pages/Forms/Login';
// import Reset from './Pages/Forms/Reset';
// const Layout = React.lazy(() => import("./Layout/Layout"));
import Login from './Pages/Forms/Login';
import Reset from './Pages/Forms/Reset';
import LoginLayout from './Layout/LoginLayout';
import ChangePassword from './Pages/Forms/ChangePassword';

const PageNotFound = React.lazy(() => import("./Pages/PageNotFound/PageNotFound"));

function App() {

  const vcipDetails = useSelector(state => state.VcipReducer.vcipDetails)

  return (
    <>
      <Toaster />
      <Suspense fallback={<div>Loading...</div>}>
        <Routes>
          <Route path='/' element={<LoginLayout />}>
            <Route index element={<Login />} />
            <Route path='reset' element={<Reset />} />
            <Route path='/resetpassword/token=:token' element={<ChangePassword />} />
          </Route>
          <Route path='/Auditor' element={<DashboardLayout />}>
            <Route element={<Dashboard />}>
              <Route path='pending' index element={<Pending />} />
              <Route path='completed' element={<Completed />} />
            </Route>
          </Route>
          <Route path='*' element={<PageNotFound />} />
        </Routes>
        <Outlet />
      </Suspense>
    </>
  );
}

export default App;
