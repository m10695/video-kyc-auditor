import { ACTION_POST_USER_PROFILE_DATA, ACTION_GET_DASHBOARD_VCIPID_COUNT_REQ, ACTION_GET_LOCATION_DETAILS_REQ, ACTION_GET_NOTIFICATIONS_LIST_REQ, APISTATUS } from "./SagaActionTypes"

export const actionReqResStatusLoaderSagaAction = (payload) => {
    return {
        type: APISTATUS,
        payload: payload
    }
}

export const actionLocationDetails = (payload) => {
    return {
        type: ACTION_GET_LOCATION_DETAILS_REQ,
        payload: payload
    }
}

export const actionNoficationListSaga = (payload) => {
    return {
        type: ACTION_GET_NOTIFICATIONS_LIST_REQ,
        payload: payload
    }
}


// GET VCIPID COUNT
export const getDashboardVCIPCountSagaAction = (payload) => {
    return {
        type: ACTION_GET_DASHBOARD_VCIPID_COUNT_REQ,
        payload: payload
    }
}

export const getUserProfileSagaAction = (payload) => {
    return {
        type: ACTION_POST_USER_PROFILE_DATA,
        payload: payload
    }
}