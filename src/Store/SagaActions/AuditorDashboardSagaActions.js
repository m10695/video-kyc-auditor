import {
    ACTION_GET_DASHBOARD_INFO_REQ,
    ACTION_GET_DASHBOARD_PENDING_REQ,
    ACTION_GET_DASHBOARD_COMPLETED_REQ,
    ACTION_GET_DASHBOARD_VCIPID_DETAILS_REQ,
    ACTION_GET_DASHBOARD_UPDATE_VCIPIDSTATUS_AUDITOR_REQ
} from "./actionTypes"

// LOGIN
export const getDashboardDataSagaAction = (payload) => {
    return {
        type: ACTION_GET_DASHBOARD_INFO_REQ,
        payload: payload
    }
}

// GET PENDING
export const getDashboardPendingSagaAction = (payload) => {
    return {
        type: ACTION_GET_DASHBOARD_PENDING_REQ,
        payload: payload
    }
}

// GET COMPLETED
export const getDashboardCompletedSagaAction = (payload) => {
    return {
        type: ACTION_GET_DASHBOARD_COMPLETED_REQ,
        payload: payload
    }
}
// GET VCIPID DETAILS
export const getDashboardVCIPDetailsSagaAction = (payload) => {
    return {
        type: ACTION_GET_DASHBOARD_VCIPID_DETAILS_REQ,
        payload: payload
    }
}

// UPDATE VCIPSTATUS BY AUDITOR
export const getUpdateVCIPIDStatusByAuditorSagaAction = (payload) => {
    return {
        type: ACTION_GET_DASHBOARD_UPDATE_VCIPIDSTATUS_AUDITOR_REQ,
        payload: payload
    }
}

