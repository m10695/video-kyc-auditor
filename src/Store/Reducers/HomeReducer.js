import * as SagaActionTypes from '../SagaActions/SagaActionTypes'

const initial = {
    isLoading: false,
    apiStatus: 0,
    pendingListCount: 0,
    completedListCount: 0
}

const HomeReducer = (state = initial, action) => {
    switch (action.type) {
        case SagaActionTypes.APISTATUS:
            if (action.payload) {
                return { ...state, apiStatus: state.apiStatus + 1 }
            } else {
                return { ...state, apiStatus: state.apiStatus - 1 }
            }

        case SagaActionTypes.ACTION_GET_DASHBOARD_VCIPID_COUNT_REQ:
            if (action.payload.listtype == "1") {
                return { ...state, pendingListCount: action.payload.count }
            } else {
                console.log(action.payload.count, 'cou')
                return { ...state, completedListCount: action.payload.count }
            }

        default:
            return state;
    }
}

export default HomeReducer;