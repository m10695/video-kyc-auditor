import * as SagaActionTypes from '../SagaActions/SagaActionTypes'

const initial = {
    vcipDetails: {},
    videoSessionData: {},
}

const VcipReducer = (state = initial, action) => {
    switch (action.type) {
        case SagaActionTypes.ACTION_GET_CUSTOMER_VCIP_DETAILS_RES:
            return { ...state, vcipDetails: action.payload }

        case SagaActionTypes.ACTION_CREATE_VIDEO_TOKEN_RES:
            return { ...state, videoSessionData: action.payload }

        default:
            return state;
    }
}

export default VcipReducer;