import toast from 'react-hot-toast';
import { put, takeLatest, call, takeEvery } from 'redux-saga/effects';
import Axios from '../../Service/axios'
import { ACTION_GET_DASHBOARD_INFO_REQ, ACTION_GET_DASHBOARD_UPDATE_VCIPIDSTATUS_AUDITOR_REQ, ACTION_GET_DASHBOARD_VCIPID_DETAILS_REQ, ACTION_GET_DASHBOARD_PENDING_REQ, ACTION_GET_DASHBOARD_COMPLETED_REQ, ACTION_GET_ROLES_REQ, ACTION_POST_LOGIN_REQ } from '../SagaActions/actionTypes';
import { actionReqResStatusLoaderSagaAction, getDashboardVCIPCountSagaAction } from '../SagaActions/CommonSagaActions';

// DASHABORD INFO
const getDashboardInfoReq = (model) => {
    const URL = "GetDashBoardCount";
    return Axios.post(URL, model).then(res => { return res?.data })
}

function* getDashboardInfoReqSaga(action) {
    yield put(actionReqResStatusLoaderSagaAction(true));
    try {
        const resp = yield call(getDashboardInfoReq, action?.payload?.model);
        if (resp && resp?.respcode === "200") {
            if (action?.payload?.callback) {
                action?.payload?.callback(resp);
            }
        } else {
            toast.error(resp?.respdesc);
        }
    } catch (err) {
        if (err.response) {
            toast.error(err?.response?.data?.errors?.length && err?.response?.data?.errors[0]?.message);
        } else {
            toast.error(err.message);
        }
    } finally {
        yield put(actionReqResStatusLoaderSagaAction(false));
    }
}

// DASHBOARD LIVE
const getDashboardPendingReq = (model) => {
    const URL = "AuditorVcipIdList";
    return Axios.post(URL, model).then(res => { return res?.data })
}

function* getDashboardPendingReqSaga(action) {
    // yield put(actionReqResStatusLoaderSagaAction(true));
    try {
        const resp = yield call(getDashboardPendingReq, action?.payload?.model);
        if (resp) {
            if (action?.payload?.callback) {
                action?.payload?.callback(resp?.vciplist);
                const payload = {
                    listtype: 1,
                    count: resp?.vciplistcount
                }
                yield put(getDashboardVCIPCountSagaAction(payload))
            }
        } else {
            toast.error(resp?.respdesc);
        }
    } catch (err) {
        if (err.response) {
            toast.error(err?.response?.data?.errors?.length && err?.response?.data?.errors[0]?.message);
        } else {
            toast.error(err.message);
        }
    } finally {
        yield put(actionReqResStatusLoaderSagaAction(false));
    }
}

// DASHBOARD SCHEDULE
const getDashboardCompletedReq = (model) => {
    const URL = "AuditorVcipIdList";
    return Axios.post(URL, model).then(res => { return res?.data })
}

function* getDashboardCompletedReqSaga(action) {
    yield put(actionReqResStatusLoaderSagaAction(true));
    try {
        const resp = yield call(getDashboardCompletedReq, action?.payload?.model);
        if (resp) {
            if (action?.payload?.callback) {
                action?.payload?.callback(resp?.vciplist);
                const payload = {
                    listtype: 2,
                    count: resp?.vciplistcount
                }
                yield put(getDashboardVCIPCountSagaAction(payload))

            }
        } else {
            toast.error(resp?.respdesc);
        }
    } catch (err) {
        if (err.response) {
            toast.error(err?.response?.data?.errors?.length && err?.response?.data?.errors[0]?.message);
        } else {
            toast.error(err.message);
        }
    } finally {
        yield put(actionReqResStatusLoaderSagaAction(false));
    }
}

// DASHBOARD REPORT DETAILSF
const getDashboardVCIPIdDetailsReq = (model) => {
    const URL = "GetVCIPIDDetails";
    return Axios.post(URL, model).then(res => { return res?.data })
}

function* getDashboardVCIPIdDetailsReqSaga(action) {
    yield put(actionReqResStatusLoaderSagaAction(true));
    try {
        const resp = yield call(getDashboardVCIPIdDetailsReq, action?.payload?.model);
        if (resp) {
            if (action?.payload?.callback) {
                action?.payload?.callback(resp);
            }
        } else {
            toast.error(resp?.respdesc);
        }
    } catch (err) {
        if (err.response) {
            toast.error(err?.response?.data?.errors?.length && err?.response?.data?.errors[0]?.message);
        } else {
            toast.error(err.message);
        }
    } finally {
        yield put(actionReqResStatusLoaderSagaAction(false));
    }
}
// UPDATED VCIPID BY AUDITOR
const getDashboardVCIPIDStatusByAuditorReq = (model) => {
    const URL = "UpdateVCIPIDStatusByAuditor";
    return Axios.post(URL, model).then(res => { return res?.data })
}

function* getDashboardVCIPIDStatusByAuditorSaga(action) {
    yield put(actionReqResStatusLoaderSagaAction(true));
    try {
        const resp = yield call(getDashboardVCIPIDStatusByAuditorReq, action?.payload?.model);
        if (resp) {
            if (action?.payload?.callback) {
                action?.payload?.callback(resp);
                toast.success(resp?.respdesc);

            }
        } else {
            toast.error(resp?.respdesc);
        }
    } catch (err) {
        if (err.response) {
            toast.error(err?.response?.data?.errors?.length && err?.response?.data?.errors[0]?.message);
        } else {
            toast.error(err.message);
        }
    } finally {
        yield put(actionReqResStatusLoaderSagaAction(false));
    }
}


export default function* AuditorDashboardWatcherSaga() {
    yield takeLatest(ACTION_GET_DASHBOARD_INFO_REQ, getDashboardInfoReqSaga);
    yield takeEvery(ACTION_GET_DASHBOARD_PENDING_REQ, getDashboardPendingReqSaga);
    yield takeEvery(ACTION_GET_DASHBOARD_COMPLETED_REQ, getDashboardCompletedReqSaga);
    yield takeEvery(ACTION_GET_DASHBOARD_VCIPID_DETAILS_REQ, getDashboardVCIPIdDetailsReqSaga);
    yield takeEvery(ACTION_GET_DASHBOARD_UPDATE_VCIPIDSTATUS_AUDITOR_REQ, getDashboardVCIPIDStatusByAuditorSaga);
}