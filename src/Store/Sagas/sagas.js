import { all } from 'redux-saga/effects';
import AuditorDashboardWatcherSaga from './AuditorDashboardWatcherSaga';
// import AdharWatcherSaga from './AdharWatcherSaga';
// import AgentDashboardWatcherSaga from './AgentDashboardWatcherSaga';
import AuthWatcherSaga from './AuthWatcherSaga';
import CommonWatcherSaga from './CommonWatcherSaga';
// import getVcipDetailsWatcherSaga from './GetVcipDetailsWatcherSaga';
// import InitiateVCWatcherSaga from './InitiateVCWatcherSaga';
// import PanWatcherSaga from './PanWatcherSaga';
// import VideoWatcherSaga from './VideoWatcherSaga';

export default function* rootSage() {
    yield all([
        CommonWatcherSaga(),
        // getVcipDetailsWatcherSaga(),
        // AdharWatcherSaga(),
        // PanWatcherSaga(),
        // InitiateVCWatcherSaga(),
        // VideoWatcherSaga(),
        
        // agent
        AuthWatcherSaga(),
        AuditorDashboardWatcherSaga()
    ])
}