import toast from 'react-hot-toast';
import { put, takeLatest, call, takeEvery } from 'redux-saga/effects';
import Axios from '../../Service/axios'
import { ACTION_STATUSBAR_UPDATE, ACTION_POST_USER_PROFILE_DATA } from '../SagaActions/actionTypes';
import { actionReqResStatusLoaderSagaAction } from '../SagaActions/CommonSagaActions';


// function* statusbarUpdateReqSaga(action) {
//     yield put(getstatusbarUpdateSagaAction(action?.payload))
// }

const getUserProfileReq = () => {
    const URL = 'UserProfile';
    return Axios.post(URL).then(res => { return res?.data })
}

function* getUserProfileReqSaga(action) {
    yield put(actionReqResStatusLoaderSagaAction(true));
    try {
        const resp = yield call(getUserProfileReq);
        if (resp?.respcode === '200') {
            if (action?.payload?.callback) {
                action?.payload?.callback(resp);
            } else {
                toast.error(resp?.respdesc);
            }
        }
    } catch (err) {
        if (err.response) {
            toast.error(err?.response?.data?.errors?.length && err?.response?.data?.errors[0]?.message);
        } else {
            toast.error(err.message);
        }
    }
     finally {
        yield put(actionReqResStatusLoaderSagaAction(false));
    }
}

// function* endVideoCallByAgentReqSaga(action) {
//     yield put(endVideoCallByAgentSagaAction(action?.payload));
// }

export default function* CommonWatcherSaga() {
    // yield takeLatest(ACTION_STATUSBAR_UPDATE, statusbarUpdateReqSaga);
    yield takeLatest(ACTION_POST_USER_PROFILE_DATA, getUserProfileReqSaga);
    // yield takeLatest(ACTION_END_VIDEO_CALL_BY_AGENT_REQ, endVideoCallByAgentReqSaga);
}