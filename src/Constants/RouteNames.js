const RouteList = {
    LOGIN: '/',
    RESET: '/reset',
    PENDING: '/Auditor/pending',
    COMPLETED: '/Auditor/completed',
    // SELECT_KYC_PROCESS: '/select-kyc-process',
    // AADHAR_KYC_KYC_DIGILOCCKER: '/aadhaar-digilocker',
    // AADHAR_KYC_PROCESS: '/aadhaar-kyc-process',
    // AADHAR_KYC_PROCESS_UPLOAD: '/aadhaar-kyc-process/upload',
    // AADHAR_KYC_PROCESS_DOWNLOAD: '/aadhaar-kyc-process/downloaded',
    // PAN_KYC_PROCESS: '/pan-kyc-process',
    // SELECT_VIDEOCALL_LANGAUAGE: '/select-video-call-language',
    // TOKEN_NUMBER: '/token-number',
    // RESCHEDULE: '/reschedule',
    // INITIATE_VIDEO_CALL: '/session/:id',
    // KYC_PROCESS_COMPLETED: '/kyc-process-completed'
}

const RouteNames = Object.freeze(RouteList);

export default RouteNames;