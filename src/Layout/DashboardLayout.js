import React, { useEffect, useState } from 'react';
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from 'chart.js';
import { Doughnut } from 'react-chartjs-2';
import { useDispatch } from 'react-redux';
import { Outlet } from 'react-router-dom';
// import { getDashboardDataSagaAction } from '../Store/SagaActions/AgentDashboardSagaActions';
import { getDashboardDataSagaAction } from '../Store/SagaActions/AuditorDashboardSagaActions';



import Header from '../Pages/Common/Header';
ChartJS.register(ArcElement, Tooltip, Legend);

const DashboardLayout = () => {

    const [dashboardData, setDashboardData] = useState({});
    const [chartData, setChartData] = useState({
        labels: ['Rejected', 'issued', 'Accepted'],
        datasets: [
            {
                label: '# of Votes',
                data: [2, 1, 2],
                backgroundColor: [
                    '#4DAE68',
                    '#F9C717',
                    '#F24747',
                ],
                borderWidth: 1,
            },
        ],
    });

    const dispatch = useDispatch();

    useEffect(() => {
        getDashboardData('1');
    }, [])

    const getDashboardData = (type) => {
        const model = {
            reqtype: type
        }
        dispatch(getDashboardDataSagaAction({ model: model, callback: getDashboardDatares }));
    }

    const getDashboardDatares = (data) => {
        // const countArr = [data?.rejectedcount, data?.issuedcount, data?.acceptedcount]
        const countArr = [data?.rejectedcount !== "0" ? data?.rejectedcount : 2, data?.issuedcount !== "0" ? data?.issuedcount : 3, data?.acceptedcount]

        const chartObj = {
            labels: ['Rejected', 'issued', 'Accepted'],
            datasets: [
                {
                    label: '# of Votes',
                    data: countArr,
                    backgroundColor: [
                        '#F24747',
                        '#F9C717',
                        '#4DAE68',
                    ],
                    borderWidth: 1,
                },
            ],
        };
        setDashboardData(data);
        setChartData(chartObj);
    }

    return (
        <>
            <Header />
            <main className="main py-3">

                <div className='container-fluid mt-3'>

                    <div className="row ">
                        <div className="col-lg-3 ">
                            <div className="bx p-4">
                                <div className="bx-hdr">
                                    <h5 className="bx-hdr-ttl">My Activity</h5>
                                    <div>
                                        <label className="bx-hdr-lbl">Show:</label>
                                        <select name="show" className="bx-hdr-inp">
                                            <option value>Today</option>
                                            <option value>Week</option>
                                            <option value>Month</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="bx-cll-chrt mt-3">
                                    <Doughnut data={chartData} />
                                </div>
                                <ul className="bx-cll-lst">
                                    <li className="bx-cll-lst-itm">
                                        <p className="bx-cll-lst-itm-nm">
                                            <span className="itm-green" />
                                            Approved
                                        </p>
                                        <span className="bx-cll-lst-itm-cnt">{dashboardData?.acceptedcount}</span>
                                    </li>
                                    <li className="bx-cll-lst-itm">
                                        <p className="bx-cll-lst-itm-nm">
                                            <span className="itm-red" />
                                            Rejected
                                        </p>
                                        <span className="bx-cll-lst-itm-cnt">{dashboardData?.rejectedcount}</span>
                                    </li>
                                    <li className="bx-cll-lst-itm">
                                        <p className="bx-cll-lst-itm-nm">
                                            <span className="itm-yellow" />
                                            Issues
                                        </p>
                                        <span className="bx-cll-lst-itm-cnt">{dashboardData?.issuedcount}</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-md-9 ">
                            <div className="bx p-4 box-2">

                                <h6>
                                    KYC Verification Queue
                                </h6>

                                <div className='line'>

                                </div>

                                {/* <ul className="nav nav-tabs custom-dshbd-tabs" id="myTab" role="tablist">
                                <li className="nav-item" role="presentation">
                                    <button className="nav-link active" id="queue-tab" data-bs-toggle="tab" data-bs-target="#queue" type="button" role="tab" aria-controls="home" aria-selected="true">Customer Queue</button>
                                </li>
                                <li className="nav-item" role="presentation">
                                    <button className="nav-link" id="history-tab" data-bs-toggle="tab" data-bs-target="#history" type="button" role="tab" aria-controls="profile" aria-selected="false">Call history</button>
                                </li>
                            </ul> */}
                                <div className="customer-main mt-4">

                                    <div className="tab-content customer-table mt-4" id="myTabContent">
                                        <Outlet />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </main>
        </>
    )
}

export default DashboardLayout;