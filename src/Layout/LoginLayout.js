import React from 'react'
import { Outlet } from 'react-router-dom'

const LoginLayout = () => {
    return (
        <>
            <article className="lgn">
                <div className="lgn-lf">
                    <div className="lgn-bx">
                        <img src="../images/logo-icon.svg" alt="logo" className="lgn-bx-icn" />
                        <div className="lgn-bx-dt">
                            <h1 className="lgn-bx-ttl">Syntizen</h1>
                            <p className="lgn-bx-tx">Solutions for all your company needs</p>
                        </div>
                    </div>
                    <img src="../images/lgn-layer.svg" alt="lgn-layer" className="lgn-layer" />
                </div>
                <div className="lgn-rt center">
                    <Outlet />
                </div>
            </article>
        </>
    )
}

export default LoginLayout